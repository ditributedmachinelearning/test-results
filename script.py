import pandas as pd
df1 = pd.read_csv("final_output.csv")
df2 = pd.read_csv("ra_dec.csv")
df3 = pd.read_csv("ra_dec2.csv")
list_ = []
list_.append(df2)
list_.append(df3)
concat = pd.concat(list_)
concat.to_csv("inbetween.csv", index=False)
df4 = pd.read_csv("inbetween.csv")
merged = df1.merge(df4, on=['ObjID','class','g','i','r','u','z'], how="left", sort=True).fillna("MISSING")
merged2 = merged.drop_duplicates(cols=['ObjID'])
merged2.to_csv("merged.csv", index=False)



