# README #

This repository contains the relevant source code and results for our paper on 'Distributed Machine Learning Techniques to Identify Quasars in Large Photometric Datasets'. It attempts to distribute classification algorithms of kNN and SVM so that they could be ran with very large test data. We used the photometric data from the SDSS III catalog to identify quasars among the unclassified objects. This data is significant because it consists of photometric data for 1,183,850,913 objects out of which only 3,751,358 objects are classified as a star, quasar or galaxy. Finally, we combined the algorithms to design an ensemble algorithm that could process the entire training dataset of 3,751,358 objects and classified a sample of 500,000 of the unclassified objects.

### What is this repository for? ###

It contains the source code for the single node kNN and SVM algorithms and the distributed kNN and SVM algorithms and also the final ensemble algorithm. It also contains the results of the classification of the unclassified objects using the ensemble algorithm.

### How do I get set up? ###

* Download our source code and input data files
* Download and set up dispy according to the instructions on the website.
* Run the dispy server
* Use python to run the source script of your choice
* The results will be printed on the screen

### How do I interpret the output? ###
The file 'final_output.csv' consists of the output of the classification in the following order:
**Objnum,ObjID,class,g,i,r,u,z,Predicted Class**
where,  
Objnum is a counter variable for the object number  
ObjID is the object ID as identified by SDSS  
class is the class as identified by SDSS which is null as the objects are not classified by them  
g is the color of the object measured in the green filter (provided by SDSS)  
i is the color of the object measured in the infrared filter (provided by SDSS)  
r is the color of the object measured in the red filter (provided by SDSS)  
u is the color of the object measured in the ultraviolet filter (provided by SDSS)  
z is the color of the object measured in the infrared filter (provided by SDSS)  
Predicted Class is the class of the object predicted by our algorithm as a QSO (quasar) or a NQSO (non-quasar)

### Who do I talk to? ###

* Anisha Nazareth (anisha.nazareth@iiitb.org)
* Anusha P S (anusha.ps@iiitb.org)
* Shrisha Rao (shrao@ieee.org)